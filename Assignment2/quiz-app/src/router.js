import Vue from "vue";
import VueRouter from "vue-router";
import StartScreen from "./components/StartScreen";
import QuestionScreen from "./components/QuestionScreen";
import ScoreScreen from "./components/ScoreScreen";
Vue.use(VueRouter);

const routes = [
	{
		path: "/", // path: "/configure",
		// alias: "/",
		component: StartScreen,
	},
	{
		path: "/question/:id",
		component: QuestionScreen,
	},
	{
		path: "/score-page",
		component: ScoreScreen,
	},

	// TODO: Lazy loading
];

const router = new VueRouter({ routes });

export default router;
