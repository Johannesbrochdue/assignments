// Vuex store.
// The store provides all necessairy functions and state
// for a component to update and fetch current data.

import Vue from "vue";
import Vuex from "vuex";
import { QuizAPI } from "../components/QuizAPI";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		questions: [],
		answers: [],
		categories: [],
		selectedCategory: 0,
		difficulty: "",
		numberOfQuestions: 0,
		nextQuestionIndex: 0,
		score: 0,
		error: "",
	},
	mutations: {
		setQuestions: (state, payload) => (state.questions = payload),
		updateNextQuestionIndex: (state) => state.nextQuestionIndex++,
		setAnswer: (state, payload) => state.answers.push(payload),
		setCategories: (state, payload) => (state.categories = payload),
		setSelectedCategory: (state, payload) => {
			state.selectedCategory = payload;
			console.log("FROM STATE: CATEGORY", state.selectedCategory);
		},
		setDifficulty: (state, payload) => (state.difficulty = payload),
		setNumberOfQuestionsState: (state, payload) =>
			(state.numberOfQuestions = payload),
		updateScore: (state) => {
			state.score = state.questions.reduce(
				(previous, current, index) => {
					if (
						current.correct_answer ===
						state.answers[index]
					) {
						return 10;
					}
					return previous;
				},
				0
			);
		},
		clearState: (state) => {
			state.questions = [];
			state.answers = [];
			state.categories = [];
			state.selectedCategory = 0;
			state.difficulty = "";
			state.numberOfQuestions = 0;
			state.nextQuestionIndex = 0;
			state.score = 0;
			state.error = "";
		},
		makeStateReadyForReplay: (state) => {
			state.questions = [];
			state.answers = [];
			state.nextQuestionIndex = 0;
		},
		setError: (state, payload) => (state.error = payload),
	},
	getters: {
		getQuestions(state) {
			return state.questions;
		},
		getAnswers(state) {
			return state.answers;
		},
		getNextQuestionIndex(state) {
			return state.nextQuestionIndex < state.questions.length
				? state.nextQuestionIndex
				: false;
		},
		getCategories(state) {
			return state.categories;
		},
		getNextQuestion(state, commit) {
			if (state.nextQuestionIndex < state.questions.length) {
				console.log(state.questions);
				return state.questions[state.nextQuestionIndex];
			} else {
				commit("setError", "No further questions");
			}
		},
		getScore(state) {
			return state.score;
		},
		getSelectedDifficulty(state) {
			console.log(state.difficulty);
			return state.difficulty;
		},
		getNumberOfQuestions(state) {
			return state.numberOfQuestions;
		},
		getSelectedCategory(state) {
			return state.selectedCategory;
		},
	},
	actions: {
		async updateQuestions({ state, commit }) {
			try {
				await QuizAPI.fetchQuestions(
					state.difficulty,
					state.numberOfQuestions,
					state.selectedCategory
				).then((res) => {
					console.log(res);
					commit("setQuestions", res);
				});
			} catch (e) {
				console.log(e.message);
				commit("setError", e.message);
			}
		},
		async updateCategories({ commit }) {
			try {
				console.log("updating categories");
				await QuizAPI.fetchCategories().then((res) => {
					console.log("FROM UPDATE CATEGORIES", res);
					commit("setCategories", res);
				});
			} catch (e) {
				commit("setError", e.message);
				console.log("UPDATE CATEGORY ERROR: ", e);
			}
		},
		updateSelectedCategory({ commit }, category) {
			commit("setSelectedCategory", category);
		},
		updateDifficulty({ commit }, difficulty) {
			commit("setDifficulty", difficulty);
		},
		updateNumberOfQuestions({ commit }, numberOfQuestions) {
			commit("setNumberOfQuestionsState", numberOfQuestions);
		},
		clearState({ commit }) {
			commit("clearState");
		},
		makeStateReadyForReplay({ commit }) {
			commit("makeStateReadyForReplay");
		},
	},
});
