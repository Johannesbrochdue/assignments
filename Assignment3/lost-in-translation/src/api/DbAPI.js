let baseUrl = "http://localhost:8000/users"

export async function getUser(username) {
    return fetch(`${baseUrl}?username=${username}`)
        .then(async (response) => {
            if (!response.ok) {
                const {error = 'Something utterly horrible has happened'} = await response.json()
                throw new Error(error)
            }
            return response.json()
        })
}

export async function createUser(username) {
    let headers = {'Content-Type': 'application/json'}
    let body = JSON.stringify({'username': username, 'translations': []})
    console.log(username)
    console.log(body)
    return fetch(baseUrl, {
        method: 'POST',
        headers: headers,
        body: body
    }).then(r => r.json())
}

export async function saveSentenceToDb(username, sentence) {
    const user = await getUser(username)
    const {id, translations} = user[0]
    translations.push(sentence)
    if (translations.length > 10) {
        await translations.shift()
    }
    return updateUser(id, {
        username: username,
        translations: translations
    }).then(r => r.json())
}

export async function fetchTranslationsFromDb(username) {
    const user = await getUser(username)
    console.log(user[0].translations)
    return user[0].translations
}

export async function updateUser(id, body) {
    let headers = {'Content-Type': 'application/json'}
    return fetch(`${baseUrl}/${id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(body)
    })
}