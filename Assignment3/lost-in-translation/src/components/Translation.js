import Sign from "./common/Sign";

const validChar = char => {
    return Boolean(char.match(/[a-z]/))
}

const getSigns = sentence => {
    return sentence.split('').map((char) => {
        if (validChar(char)) {
            return `/sign/${char}.png`
        } else {
            return ''
        }
    })
}

const Translation = props => {
    const signs = getSigns(props.sentence)
    console.log("from translation: ", props.sentence)

    return (
        <div className='container row'>
            {signs.map((src, index) => {
                return src ? <Sign src={src} alt={signs[index]}/> : <div className='row'/>
            })}
        </div>
    )
}

export default Translation