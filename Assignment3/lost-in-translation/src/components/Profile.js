import {sessionLogoutAction} from "../store/acions/sessionActions";
import CenteredButton from "./common/CenteredButton";
import {useDispatch, useSelector} from "react-redux";
import Translation from "./Translation";


const Profile = () => {
    const dispatch = useDispatch()
    const {username} = useSelector(state => state.session)
    const {translations} = useSelector(state => state.translation)
    console.log(translations)

    const onLogOutClick = () => {
        dispatch(sessionLogoutAction())
    }

    return (
        <>
            {translations && translations.map((it) => {
                console.log("sentence from Profile", it + "  ")

                return <><div className='lg-space'/><Translation sentence={it}/></>
            })}
            <CenteredButton text="LOGOUT" onSubmit={onLogOutClick}/>
        </>

    )
}

export default Profile