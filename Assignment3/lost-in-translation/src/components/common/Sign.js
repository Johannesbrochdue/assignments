const Sign = props => {
    return (
        <div className='col-sm-1'>
            <img src={props.src} alt={props.alt} width="100%"/>
        </div>
    )
}

export default Sign