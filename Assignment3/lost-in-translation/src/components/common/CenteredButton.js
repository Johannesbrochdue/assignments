const CenteredButton = props => {
    return (
        <>
            <div className='form-row ms-5 me-5' align='center'>
                <button
                    className='btn btn-lg btn-primary primary mt-3'
                    onClick={props.onSubmit}>{props.text.toUpperCase()}
                </button>
            </div>
        </>
    )
}

export default CenteredButton