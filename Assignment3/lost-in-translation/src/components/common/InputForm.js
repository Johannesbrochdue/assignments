const InputForm = props => {
    return (
        <>
            <div className='form-row ms-5 me-5 mt-lg-5 mb-3'>
                <input type="text"
                       className='form-control'
                       id={props.id}
                       placeholder={props.placeholder}
                       onChange={props.onChange}
                />
            </div>
        </>
    )
}

export default InputForm