import {Link} from "react-router-dom";
import "../css/NavBar.css"
import {useDispatch, useSelector} from "react-redux";
import AppContainer from "../hoc/AppContainer";
import {translationFetchAction} from "../store/acions/translationActions";

export const NavBar = () => {
    const {username, loggedIn} = useSelector(state => state.session)
    const session = useSelector(state => state.session)
    const dispatch = useDispatch()
    dispatch(translationFetchAction(username))
    return (
        <>
            <div className='space secondary'/>
            <AppContainer>

                <nav className='navbar navbar-expand-md primary'>
                    <ul className="nav nav-pills nav-justified">
                        <li className="nav-item">
                            <Link className='nav-link' to='/startup-page'>
                                <img src="./Logo-Hello.png" alt="Logo" id='nav-logo'/>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <h1 className="nav-item" to="#">Lost in Translation</h1>
                        </li>
                        {loggedIn && <li className='nav-item'>
                            <Link className="nav-link" to='/profile-page'><h4>Welcome {username}</h4></Link>
                        </li>}
                    </ul>


                </nav>
            </AppContainer>
        </>
    );
}