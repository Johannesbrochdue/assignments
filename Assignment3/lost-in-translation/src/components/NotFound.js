import {Link} from "react-router-dom";

const NotFound = () => {
    return (
        <div className='alert alert-danger'>
            <h1>Somehow you have stepped outside of our website...</h1>
            <p>To prevent you from falling into the abyss, grab this rope:</p>
            <Link to='/startup-page'>Home</Link>
        </div>
    )
}
export default NotFound