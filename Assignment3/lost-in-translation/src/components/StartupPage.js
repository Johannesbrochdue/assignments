import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {loginAttemptAction} from "../store/acions/loginActions";
import {Redirect} from "react-router-dom";
import InputForm from "./common/InputForm"
import CenteredButton from "./common/CenteredButton";
import {translationFetchAction} from "../store/acions/translationActions";

const StartupPage = () => {
    const [name, setName] = useState('')
    const {loggedIn} = useSelector(({session}) => session)
    const dispatch = useDispatch()
    const handleOnChange = event => {
        setName(event.target.value)
    }

    async function handleOnSubmit() {
        dispatch(loginAttemptAction(name))
        dispatch(translationFetchAction(name))
    }

    return (
        <>  {loggedIn && <Redirect to='/translation-page'/>}
            {!loggedIn &&
            <main>
                <InputForm id='login' placeholder='Enter your name' onChange={handleOnChange}/>
                <CenteredButton text='SUBMIT' onSubmit={handleOnSubmit}/>
            </main>
            }
        </>
    )
}

export default StartupPage