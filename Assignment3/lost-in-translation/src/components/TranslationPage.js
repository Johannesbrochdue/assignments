import {useDispatch, useSelector} from "react-redux";
import InputForm from "./common/InputForm";
import CenteredButton from "./common/CenteredButton";
import {useState} from "react";
import {translationAddAction} from "../store/acions/translationActions";
import Translation from "./Translation";



const TranslationPage = () => {
    const {username} = useSelector(state => state.session)
    const dispatch = useDispatch()
    const [sentenceToTranslate, setSentenceToTranslate] = useState('')
    const [readyToTranslate, setReadyToTranslate] = useState("")

    const handleOnChange = event => {
        event.preventDefault()
        setSentenceToTranslate(event.target.value.toLowerCase())
    }

    const translate = () => {
        dispatch(translationAddAction(username, sentenceToTranslate))
        setReadyToTranslate(sentenceToTranslate)
    }
    return(
        <main>
            <InputForm id='translation' placeholder="Type Sentence To Translate!" onChange={handleOnChange}/>
            <Translation sentence={readyToTranslate}/>
            <CenteredButton text='translate' onSubmit={translate}/>
        </main>
    )
}

export default TranslationPage