import {
    ACTION_TRANSLATION_ADD,
    translationErrorAction, translationSetAction, ACTION_TRANSLATION_FETCH, translationFetchAction
} from "../acions/translationActions";
import {fetchTranslationsFromDb, saveSentenceToDb} from "../../api/DbAPI";

export const translationMiddleware = ({dispatch}) => next => action => {
    next(action)

    if (action.type === ACTION_TRANSLATION_ADD) {
        const username = action.payload.username
        const sentence = action.payload.sentence
        saveSentenceToDb(username, sentence)
            .then(r => dispatch(translationSetAction(r.translations)))
            .catch(e => {
                dispatch(translationErrorAction(e.message))
            })
    }

    if (action.type === ACTION_TRANSLATION_FETCH) {
        fetchTranslationsFromDb(action.payload)
            .then(r => {
                dispatch(translationSetAction(r))
            })
            .catch(e => {
                dispatch(translationErrorAction(e.message))
            })
    }

}