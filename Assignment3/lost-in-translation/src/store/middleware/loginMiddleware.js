import {
    ACTION_LOGIN_ATTEMPTING,
    ACTION_LOGIN_ERROR,
    ACTION_LOGIN_SUCCESS,
    loginErrorAction, loginSuccessAction
} from "../acions/loginActions";
import {getUser, createUser} from "../../api/DbAPI"
import {sessionSetAction} from "../acions/sessionActions";

export const loginMiddleware = ({dispatch}) => next => action => {
    next(action)

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
        getUser(action.payload)
            .then(r => {
                console.log(r)
                if (r.length === 0) {
                    createUser(action.payload)
                        .then(r => {
                            console.log("USER CREATED: ", r)
                            dispatch(loginSuccessAction({username: r.username}))
                        })
                } else {
                    console.log("LOGGING USER IN")
                    console.log(r)
                    dispatch(loginSuccessAction(r[0]))
                }
            }).catch(e => {
            dispatch(loginErrorAction(e.message))
        })
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }

    if (action.type === ACTION_LOGIN_ERROR) {
        //Set error
    }
}