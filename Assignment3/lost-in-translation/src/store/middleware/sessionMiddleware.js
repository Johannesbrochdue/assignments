import {
    ACTION_SESSION_CLEAR, ACTION_SESSION_EXISTS,
    ACTION_SESSION_LOGOUT,
    ACTION_SESSION_SET,
    sessionClearAction, sessionSetAction
} from "../acions/sessionActions";
import {translationFetchAction} from "../acions/translationActions";

export const sessionMiddleware = ({dispatch}) => next => action => {
    next(action)

    if (action.type === ACTION_SESSION_SET) {
        localStorage.setItem('translate-session', JSON.stringify(action.payload))
    }

    if (action.type === ACTION_SESSION_CLEAR) {
        localStorage.removeItem('translate-session')
    }

    if (action.type === ACTION_SESSION_LOGOUT) {
        dispatch(sessionClearAction())
    }

    if (action.type === ACTION_SESSION_EXISTS) {
        let session = localStorage.getItem('translate-session')
        if (session && session !== 'undefined') {
            session = JSON.parse(session)
            console.log("SESSION SESSION SESSION:")
            console.log(session)
            dispatch(sessionSetAction(session))
        } else {
            dispatch(sessionClearAction())
        }
    }

}