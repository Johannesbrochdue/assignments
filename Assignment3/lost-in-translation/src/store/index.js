import {createStore} from "redux";
import appReducer from "./reducers";
import {composeWithDevTools} from "redux-devtools-extension";
import middleware from "./middleware"

export default createStore(
    appReducer,
    composeWithDevTools(middleware)
)