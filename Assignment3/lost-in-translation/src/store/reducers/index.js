import {combineReducers} from "redux";
import {loginReducer} from "./loginReducer";
import {sessionReducer} from "./sessionReducer";
import {ACTION_SESSION_CLEAR} from "../acions/sessionActions";
import {translationReducer} from "./translationReducer";

const appReducer = combineReducers({
    login: loginReducer,
    session: sessionReducer,
    translation: translationReducer
})

const rootReducer = (state, action) => {
    if (action.type === ACTION_SESSION_CLEAR) {
        state = undefined
    }
    return appReducer(state, action)
}

export default rootReducer