import {ACTION_TRANSLATION_SET, ACTION_TRANSLATION_ADD} from "../acions/translationActions";

const initialState = {
    translations: ''
}

export const translationReducer = (state = initialState, action) => {

    switch (action.type) {
        case ACTION_TRANSLATION_ADD:
            return {...state}

        case ACTION_TRANSLATION_SET:
            console.log("ACTION_TRANSLATION_SET")
            console.log(action.payload)
            return {
                ...initialState,
                translations: action.payload
            }

        default:
            console.log("DEFAULT TRANSLATION STATE")
            console.log(state)
            return state

    }
}