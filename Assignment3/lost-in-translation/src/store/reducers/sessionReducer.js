import {ACTION_SESSION_CLEAR, ACTION_SESSION_LOGOUT, ACTION_SESSION_SET} from "../acions/sessionActions";

const initialState = {
    loggedIn: false,
    username: '',
}

export const sessionReducer = (state = {...initialState}, action) => {
    switch (action.type) {

        case ACTION_SESSION_SET:
            console.log("SETTING STATE", action.payload)
            return {
                ...action.payload,
                loggedIn: true
            }

        case ACTION_SESSION_LOGOUT:
            console.log("LOGGING OUT")
            return {
                ...state,
                loggedIn: false
            }

        case ACTION_SESSION_CLEAR:
            console.log("SESSION CLEAR")
            return {
                ...initialState
            }

        default:
            console.log("DEFAULT SESSION STATE")
            console.log(state)
            return state
    }
}