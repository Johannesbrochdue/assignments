export const ACTION_TRANSLATION_ADD = '[translation] SAVE'
export const ACTION_TRANSLATION_FETCH = '[translation] FETCH'
export const ACTION_TRANSLATION_SET = '[translation] SET'
export const ACTION_TRANSLATION_ERROR = '[translation] ERROR'

export const translationAddAction = (username, sentence) => ({
    type: ACTION_TRANSLATION_ADD,
    payload: {
        sentence: sentence,
        username: username
    }
})

export const translationFetchAction = username => ({
    type: ACTION_TRANSLATION_FETCH,
    payload: username
})

export const translationSetAction = translations => ({
    type: ACTION_TRANSLATION_SET,
    payload: translations
})

export const translationErrorAction = message => ({
    type: ACTION_TRANSLATION_ERROR,
    payload: message
})