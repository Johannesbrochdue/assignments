export const ACTION_SESSION_SET = '[session] SET'
export const ACTION_SESSION_LOGOUT = '[session] LOGOUT'
export const ACTION_SESSION_CLEAR = '[session] CLEAR'
export const ACTION_SESSION_EXISTS = '[session] EXISTS'

export const sessionSetAction = session => ({
    type: ACTION_SESSION_SET,
    payload: session
})

export const sessionLogoutAction = () => ({
    type: ACTION_SESSION_LOGOUT
})

export const sessionClearAction = () => ({
    type: ACTION_SESSION_CLEAR
})


export const sessionExistsAction = () => ({
    type: ACTION_SESSION_EXISTS
})