import './App.css';
import AppContainer from "./hoc/AppContainer";
import {NavBar} from "./components/NavBar";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Profile from "./components/Profile";
import StartupPage from "./components/StartupPage";
import TranslationPage from "./components/TranslationPage";
import NotFound from "./components/NotFound";
import InternalRoute from "./hoc/InternalRoute";

function App() {
    return (
        <BrowserRouter>
            <AppContainer>
                <NavBar/>
                <div className='space'/>
                <Switch>
                    <Route path='/' exact>
                        <Redirect to='/startup-page'/>
                    </Route>
                    <Route path='/startup-page' exact component={StartupPage}/>
                    <InternalRoute path='/translation-page' component={TranslationPage}/>
                    <InternalRoute path='/profile-page' component={Profile}/>
                    <Route path='*' component={NotFound}/>
                </Switch>
                <div className="App">
                </div>
            </AppContainer>
        </BrowserRouter>
    );
}

export default App;