const AppContainer = props => {
    return (
        <div className='container secondary'>
            {props.children}
        </div>
    )
}

export default AppContainer