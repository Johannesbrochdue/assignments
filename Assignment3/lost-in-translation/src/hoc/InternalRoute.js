import {useSelector} from "react-redux";
import {Redirect, Route} from "react-router-dom";

const InternalRoute = props => {
    const {loggedIn} = useSelector(state => state.session)
    console.log(loggedIn)
    return loggedIn ? <Route {...props}/> : <Redirect to='/startup-page'/>
}

export default InternalRoute