//A Bank object -> contains banking-related method "getLoan", and banking related variables.
bank = {
	name: "Johannes Bank",
	currency: "Kr.",
	balance: 0,
	loan: 0,
	getLoan() {
		if (this.loan > 0) {
			alert("You must first pay back your loan! Preferably, TODAY");
			return;
		}
		const amount = prompt("Enter desired amount to loan");
		if (Number(amount) > 2 * this.balance) {
			alert(
				`You can not loan more than double your balance. \nMaximum amount you can loan: ${
					this.balance * 2
				}`
			);
		} else {
			if (isNaN(Number(amount))) return;
			this.loan = Number(amount);
			this.balance += Number(amount);
			render();
		}
	},
};

//A Work object. Contains work-section-related methods and work-related variables.
work = {
	pay: 0,
	currency: bank.currency,
	transferToBalance() {
		let savedPay = this.pay;
		this.pay = 0;
		if (bank.loan > 0) {
			if (savedPay * 0.1 > bank.loan) {
				savedPay -= bank.loan;
				bank.loan = 0;
				bank.balance += savedPay;
			} else {
				bank.loan -= savedPay * 0.1;
				bank.balance += savedPay * 0.9;
			}
			render();
		} else {
			bank.balance += savedPay;
			render();
		}
	},
	work() {
		this.pay += 100;
		render();
	},
	repayLoan() {
		if (this.pay === 0) {
			alert("You must first earn money by working");
			return;
		}
		if (this.pay < bank.loan) {
			bank.loan -= this.pay;
			this.pay = 0;
			render();
		} else {
			bank.balance += this.pay - bank.loan;
			bank.loan = 0;
			this.pay = 0;
			render();
		}
	},
};

// Array of laptops.
let laptops = []

// This function populates the selectDropDown. But does more, so probably not the perfect solution: 
// It also populates laptops array on line 73, and renders computer information.
async function populateSelectDropdownAndLaptops() {
	try {
		const url = "https://noroff-komputer-store-api.herokuapp.com/computers" 
		laptops = await fetch(url).then(res => res.json());
		laptops[4].image = "assets/images/5.png"
		laptops.map(createOptionForDropdown)
		renderComputerInformation()
	} catch (error) {
		console.error("getLaptops: ", error);
	}
}

// helper-function to determine the value stored in the dropdown, and the data that's displayed to the user.
function createOptionForDropdown(laptop) {
	let opt = document.createElement('option')
	opt.value = laptop.id
	opt.innerText = laptop.title
	if (laptop.id == 1) {
		opt.selected = true
	}
	selectDropdown.appendChild(opt)
}

//Bunch of variable declarations
const customerNameElement = document.getElementById("customerName");
const customerBalanceElement = document.getElementById("customerBalance");
const customerLoanBalanceElement = document.getElementById(
	"customerLoanBalance"
);
const customerLoanDescriptionElement = document.getElementById(
	"customerLoanDescription"
);
const getLoanButton = document.getElementById("getLoanButton");
getLoanButton.addEventListener("click", bank.getLoan.bind(bank));

const payBalanceElement = document.getElementById("payBalance");
const workDiv = document.getElementById("workDiv");
const laptopDiv = document.getElementById("laptopDiv")

const workButton = document.getElementById("workButton");
workButton.addEventListener("click", work.work.bind(work));

const bankButton = document.getElementById("bankButton");
bankButton.addEventListener("click", work.transferToBalance.bind(work));

const selectDropdown = document.getElementById("laptops-dropdown")
populateSelectDropdownAndLaptops()
selectDropdown.addEventListener('change', render)
const spec0 = document.getElementById("spec0")
const spec1 = document.getElementById("spec1")
const spec2 = document.getElementById("spec2")

const imageUrl = "https://noroff-komputer-store-api.herokuapp.com/"
const computerImage = document.getElementById("computerImage")
const computerTitle = document.getElementById("computerTitle")
const computerDescription = document.getElementById("computerDescription")

const price = document.getElementById("price")
let currentLaptop;

const buyButton = document.getElementById("buyButton")
buyButton.addEventListener('click', () => {
	console.log(currentLaptop.price)
	if (currentLaptop.price > bank.balance) {
		alert("You can't afford this computer. GO and get your ass working!")
	} else {
		alert(`You are now the owner of: ${currentLaptop.title}`)
		bank.balance -= currentLaptop.price
		render()
	}
})

// This function renders the computer-information section (bottom half + right third) of the screen.
function renderComputerInformation() {
	let laptop = laptops[Number(selectDropdown.value) - 1]
	if (typeof laptop == "undefined") {
		laptop = laptops[0]
	}
	currentLaptop = laptop
	let specs = laptop.specs
	spec0.innerText = specs[0]
	spec1.innerText = specs[1]
	spec2.innerText = specs[2]
	computerImage.src = `${imageUrl}${laptop.image}`
	console.log(computerImage.src)
	computerTitle.innerText = laptop.title
	computerDescription.innerText = laptop.description
	price.innerText = `${laptop.price} NOK`
}

// This function renders the entire screen
function render() {
	customerNameElement.innerHTML = bank.name;
	customerBalanceElement.innerHTML = bank.balance + " " + bank.currency;
	if (bank.loan > 0) {
		customerLoanBalanceElement.innerHTML =
			bank.loan + " " + bank.currency;
		customerLoanDescriptionElement.innerHTML = "Loan: ";
		let repayLoanButton = document.createElement("button");
		repayLoanButton.addEventListener(
			"click",
			work.repayLoan.bind(work)
		);
		repayLoanButton.innerText = "Repay Loan";
		repayLoanButton.className = "button";
		repayLoanButton.id = "repayLoanButton";
		workDiv.appendChild(repayLoanButton);
	} else {
		customerLoanBalanceElement.innerHTML = "";
		customerLoanDescriptionElement.innerHTML = "";
		let removeButton = document.getElementById("repayLoanButton");
		if (removeButton != null) {
			removeButton.parentNode.removeChild(removeButton);
			window.render();
		}
	}
	payBalanceElement.innerHTML = work.pay + " " + work.currency;
	renderComputerInformation()
}

render();
