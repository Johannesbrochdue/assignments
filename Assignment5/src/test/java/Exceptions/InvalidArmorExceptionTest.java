package Exceptions;

import Characters.Mage;
import Items.Armor;
import Items.ArmorType;
import Items.Slot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InvalidArmorExceptionTest {

    @Test
    @DisplayName("Assert Throws When Eqipping Higher Level Armor")
    public void equipArmor_WhenEquippingHigherLevelArmor_ThrowsInvalidArmorException() {
        Mage mage = new Mage("Mage");
        Armor armor = new Armor();
        armor.setLevel(2);
        armor.setArmorType(ArmorType.CLOTH);
        armor.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidArmorException.class, () -> {
            mage.equipArmor(armor);
        });
    }

    @Test
    @DisplayName("Assert Throws When Equipping Wrong Armor For Character")
    public void equipArmor_WhenEquippingWrongArmorForCharacter_ThrowsInvalidArmorException() {
        Mage mage = new Mage("Mage");
        Armor armor = new Armor();
        armor.setLevel(1);
        armor.setArmorType(ArmorType.LEATHER);
        armor.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidArmorException.class, () -> {
            mage.equipArmor(armor);
        });
    }

    @Test
    @DisplayName("Assert Does Not Throw When Equipping Legal Armor")
    public void equipArmor_WhenEquippingLegalArmor_DoesNotThrowException() {
        Mage mage = new Mage("The Mage Lord");
        Armor armor = new Armor();
        armor.setArmorType(ArmorType.CLOTH);
        Assertions.assertDoesNotThrow(() -> {
            mage.equipArmor(armor);
        });
    }

    @Test
    @DisplayName("Assert Returns True When Equipping Legal Armor")
    public void equipArmor_WhenEquippingLegalArmor_ReturnsTrue() throws InvalidArmorException {
        Mage mage = new Mage("The Mage Lord");
        Armor armor = new Armor();
        armor.setArmorType(ArmorType.CLOTH);
        Assertions.assertEquals(true, mage.equipArmor(armor));
    }

}