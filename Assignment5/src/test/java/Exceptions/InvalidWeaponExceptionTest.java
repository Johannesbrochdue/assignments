package Exceptions;

import Characters.Mage;
import Items.Slot;
import Items.Weapon;
import Items.WeaponType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InvalidWeaponExceptionTest {

    @Test
    @DisplayName("Assert Throws When Equipping Higher Level Weapon")
    public void equipWeapon_WhenEquippingHigherLevelWeapon_ThrowsInvalidWeaponException() {
        Mage mage = new Mage("Mage");
        Weapon weapon = new Weapon();
        weapon.setLevel(2);
        weapon.setWeaponType(WeaponType.WAND);
        weapon.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            mage.equipWeapon(weapon);
        });
    }

    @Test
    @DisplayName("Assert Throws When Equipping Wrong Weapon For Character")
    public void equipWeapon_WhenEquippingWrongWeaponForCharacter_ThrowsInvalidWeaponException() {
        Mage mage = new Mage("Mage");
        Weapon weapon = new Weapon();
        weapon.setLevel(1);
        weapon.setWeaponType(WeaponType.BOW);
        weapon.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            mage.equipWeapon(weapon);
        });
    }

    @Test
    @DisplayName("Assert Does Not Throw When Equipping Legal Weapon")
    public void equipWeapon_WhenEquippingLegalWeapon_DoesNotThrowException() {
        Mage mage = new Mage("The Mage Lord");
        Weapon weapon = new Weapon();
        weapon.setWeaponType(WeaponType.STAFF);
        Assertions.assertDoesNotThrow(() -> {
            mage.equipWeapon(weapon);
        });
    }

    @Test
    @DisplayName("Assert Returns True When Equipping Legal Weapon")
    public void equipWeapon_WhenEquipLegalWeapon_ReturnsTrue() throws InvalidWeaponException {
        Mage mage = new Mage("The Mage Lord");
        Weapon weapon = new Weapon();
        weapon.setWeaponType(WeaponType.STAFF);
        Assertions.assertEquals(true, mage.equipWeapon(weapon));
    }

}