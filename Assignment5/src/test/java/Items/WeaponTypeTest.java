package Items;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTypeTest {

    @Test
    @DisplayName("Getting name of weapon works")
    public void getNameOfWeapon() {
        String name = WeaponType.WAND.getName();
        assertEquals("WAND", name);
    }

}