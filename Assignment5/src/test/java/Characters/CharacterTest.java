package Characters;

import Attributes.Attribute;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CharacterTest {

    @Test
    @DisplayName("Assert Throws When Eqipping Higher Level Weapon")
    public void equipWeapon_WhenEquippingHigherLevelWeapon_ThrowsInvalidWeaponException() {
        Mage mage = new Mage("Mage");
        Weapon weapon = new Weapon();
        weapon.setLevel(2);
        weapon.setWeaponType(WeaponType.WAND);
        weapon.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            mage.equipWeapon(weapon);
        });
    }

    @Test
    @DisplayName("Assert Throws When Equipping Wrong Weapon For Character")
    public void equipWeapon_WhenEquippingWrongWeaponForCharacter_ThrowsInvalidWeaponException() {
        Mage mage = new Mage("Mage");
        Weapon weapon = new Weapon();
        weapon.setLevel(1);
        weapon.setWeaponType(WeaponType.BOW);
        weapon.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            mage.equipWeapon(weapon);
        });
    }

    @Test
    @DisplayName("Assert Does Not Throw When Equipping Legal Weapon")
    public void equipWeapon_WhenEquippingLegalWeapon_DoesNotThrowException() {
        Character mage = new Mage("The Mage Lord");
        Weapon weapon = new Weapon();
        weapon.setWeaponType(WeaponType.STAFF);
        Assertions.assertDoesNotThrow(() -> {
            mage.equipWeapon(weapon);
        });
    }

    @Test
    @DisplayName("Assert Returns True When Equipping Legal Weapon")
    public void equipWeapon_WhenEquipLegalWeapon_ReturnsTrue() throws InvalidWeaponException {
        Character mage = new Mage("The Mage Lord");
        Weapon weapon = new Weapon();
        weapon.setWeaponType(WeaponType.STAFF);
        Assertions.assertEquals(true, mage.equipWeapon(weapon));
    }

    @Test
    @DisplayName("Assert Throws When Eqipping Higher Level Armor")
    public void equipArmor_WhenEquippingHigherLevelArmor_ThrowsInvalidArmorException() {
        Mage mage = new Mage("Mage");
        Armor armor = new Armor();
        armor.setLevel(2);
        armor.setArmorType(ArmorType.CLOTH);
        armor.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidArmorException.class, () -> {
            mage.equipArmor(armor);
        });
    }

    @Test
    @DisplayName("Assert Throws When Equipping Wrong Armor For Character")
    public void equipArmor_WhenEquippingWrongArmorForCharacter_ThrowsInvalidArmorException() {
        Mage mage = new Mage("Mage");
        Armor armor = new Armor();
        armor.setLevel(1);
        armor.setArmorType(ArmorType.LEATHER);
        armor.setSlot(Slot.BODY);
        Assertions.assertThrows(InvalidArmorException.class, () -> {
            mage.equipArmor(armor);
        });
    }

    @Test
    @DisplayName("Assert Does Not Throw When Equipping Legal Armor")
    public void equipArmor_WhenEquippingLegalArmor_DoesNotThrowException() {
        Character mage = new Mage("The Mage Lord");
        Armor armor = new Armor();
        armor.setArmorType(ArmorType.CLOTH);
        Assertions.assertDoesNotThrow(() -> {
            mage.equipArmor(armor);
        });
    }

    @Test
    @DisplayName("Assert Returns True When Equipping Legal Armor")
    public void equipArmor_WhenEquippingLegalArmor_ReturnsTrue() throws InvalidArmorException {
        Character mage = new Mage("The Mage Lord");
        Armor armor = new Armor();
        armor.setArmorType(ArmorType.CLOTH);
        Assertions.assertEquals(true, mage.equipArmor(armor));
    }

    @Test
    @DisplayName("Character Is Level 1 When Created")
    void newCharacter_whenCreated_isLevel1() {
        Character mage = new Mage("Mage");
        int expected = 1;
        int actual = mage.getLevel();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character Is Level 2 Upon First LevelUp")
    void character_whenFirstLevelUpCreated_isLevel2() {
        Character mage = new Mage("Mage");
        mage.levelUp();
        int expected = 2;
        int actual = mage.getLevel();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newMage_whenCreated_hasProperAttributes() {
        Mage mage = new Mage("Mage");
        Attribute expected = new Attribute(5, 1, 1, 8);
        Attribute actual = mage.attributes;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newRanger_whenCreated_hasProperAttributes() {
        Ranger ranger = new Ranger("Ranger");
        Attribute expected = new Attribute(8, 1, 7, 1);
        Attribute actual = ranger.attributes;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newRogue_whenCreated_hasProperAttributes() {
        Rogue rogue = new Rogue("Rogue");
        Attribute expected = new Attribute(8, 2, 6, 1);
        Attribute actual = rogue.attributes;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newWarrior_whenCreated_hasProperAttributes() {
        Warrior warrior = new Warrior("Warrior");
        Attribute expected = new Attribute(10, 5, 2, 1);
        Attribute actual = warrior.attributes;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newMage_uponLevelUp_hasProperAttributes() {
        Mage mage = new Mage("Mage");
        mage.levelUp();
        Attribute expected = new Attribute(5 + 3, 1 + 1, 1 + 1, 8 + 5);
        Attribute actual = mage.attributes;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newRanger_uponLevelUp_hasProperAttributes() {
        Ranger ranger = new Ranger("Ranger");
        ranger.levelUp();
        Attribute expected = new Attribute(8 + 2, 1 + 1, 7 + 5, 1 + 1);
        Attribute actual = ranger.attributes;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newRogue_uponLevelUp_hasProperAttributes() {
        Rogue rogue = new Rogue("Rogue");
        rogue.levelUp();
        Attribute expected = new Attribute(8 + 3, 2 + 1, 6 + 4, 1 + 1);
        Attribute actual = rogue.attributes;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Character is created with proper default attributes")
    void newWarrior_uponLevelUp_hasProperAttributes() {
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp();
        Attribute expected = new Attribute(10 + 5, 5 + 3, 2 + 2, 1 + 1);
        Attribute actual = warrior.attributes;
        Assertions.assertEquals(expected, actual);
    }


//4) Each character class has their attributes increased when leveling up.
//    o Create each class once, level them up once.
//    o Use the base attributes, plus one instance of the level up as the expected.
//    o E.g. Warrior -> levelup(1) -> (Vitality = 15, Strength = 8, Dexterity = 4, Intelligence = 2) expected.
//    o This results in four test methods.

    @Test
    @DisplayName("Calculate DPS When No Weapon Is Equipped")
    public void calculateDPS_WhenNoWeaponIsEquipped_IsSameAsExpected() {
        Character warrior = new Warrior("The Warrior Lord");
        double expectedDPS = 1.0 * (1.0 + (5.0 / 100.0));
        double actualDPS = warrior.calculateDPS();
        Assertions.assertEquals(expectedDPS, actualDPS);
    }

    @Test
    @DisplayName("Calculate DPS When Valid Weapon Is Equipped")
    public void calculateDPS_WhenValidWeaponIsEquipped_IsSameAsExpected() {
        Character warrior = new Warrior("The Warrior Lord");
        Weapon testWeapon = getTestAxe();
        try {
            warrior.equipWeapon(testWeapon);
        } catch (InvalidWeaponException e) {
            e.printStackTrace();
        }
        double expectedDPS = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));
        double actualDPS = warrior.calculateDPS();
        Assertions.assertEquals(expectedDPS, actualDPS);
    }

    @Test
    @DisplayName("Calculate DPS When Valid Weapon & Armor Is Equipped")
    public void calculateDPS_WhenValidWeaponAndArmorIsEquipped_IsSameAsExpected() {
        Character warrior = new Warrior("The Warrior Lord");
        Weapon testWeapon = getTestAxe();
        Armor testArmor = getTestPlateBodyArmor();
        try {
            warrior.equipArmor(testArmor);
            warrior.equipWeapon(testWeapon);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            e.printStackTrace();
        }
        double expectedDPS = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0));
        double actualDPS = warrior.calculateDPS();
        Assertions.assertEquals(expectedDPS, actualDPS);
    }

    private Weapon getTestAxe() {
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setWeaponType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        return testWeapon;
    }

    private Armor getTestPlateBodyArmor() {
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setLevel(1);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttribute(new Attribute(0, 1, 0, 2));
        return testPlateBody;
    }
}