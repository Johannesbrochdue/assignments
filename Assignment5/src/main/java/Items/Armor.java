package Items;

import Attributes.Attribute;

public class Armor extends Item{
    ArmorType armorType;
    Attribute attribute;

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public int getVitality() {
        return attribute.getVitality();
    }

    public int getStrength() {
        return attribute.getStrength();
    }

    public int getDexterity() {
        return attribute.getDexterity();
    }

    public int getIntelligence() {
        return attribute.getIntelligence();
    }
}
