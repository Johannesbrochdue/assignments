package Items;

public class Weapon extends Item {
    private int damage;
    private double attackSpeed;
    private WeaponType weaponType;

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getDamage() {
        return damage * attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public void setWeaponType(WeaponType weapon) {
        this.weaponType = weapon;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }


}