// Super Class for Weapon and Armor
package Items;

abstract public class Item {
    private String name;
    private int level;
    private Slot slot;

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public int getLevel() {
        return level;
    }

    public Slot getSlot() {
        return slot;
    }
}
