package Exceptions;

public class InvalidWeaponException extends Throwable {
    public InvalidWeaponException(String message) {
        super(message);
    }
}
