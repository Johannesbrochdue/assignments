package Characters;

import Attributes.Attribute;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;

public class Warrior extends Character {

    public Warrior(String name) {
        super(name, new Attribute(10, 5, 2, 1));
    } // Super Class handles level check

    @Override
    int getAppropriateAttributeFromArmor(Armor armor) {
        return armor.getStrength();
    }

    @Override
    int getPrimaryAttributeFromAttributes() {
        return attributes.getStrength();
    }

    @Override
    void levelUp() {
        level++;
        attributes.increaseVitality(5);
        attributes.increaseStrength(3);
        attributes.increaseDexterity(2);
        attributes.increaseIntelligence(1);
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        super.equipWeapon(weapon); // Super Class handles level check
        WeaponType weaponType = weapon.getWeaponType();
        if (weaponType != WeaponType.AXE && weaponType != WeaponType.SWORD && weaponType != WeaponType.HAMMER) {
            throw new InvalidWeaponException("A warrior can only equip AXE's, SWORD's and HAMMER's as weapon");
        }
        items.put(weapon.getSlot(), weapon);
        return true;
    }

    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        super.equipArmor(armor); // Super Class handles level check
        if (armor.getArmorType() != ArmorType.MAIL && armor.getArmorType() != ArmorType.PLATE) {
            throw new InvalidArmorException("Warrior can only equip ArmorType MAIL and ArmorType PLATE");
        }
        items.put(armor.getSlot(), armor);
        return true;
    }
}
