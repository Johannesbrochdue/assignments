**CHARACTERS**

The **Character** class is the superclass of the other character classes.

It is responsible for calculating DPS, and partly responsible for equipping items.

The calculateDPS method, when needing specific Character data,
polymorphism is used. These methods are "getPrimaryAttributeFromAttributes",
and "getPrimaryAttributeFromArmor"

In the equipMethod, the Super class only checks whether the item is equippable 
given the Character's level. Each sub class has its own 
implementation, that calls the parent's method first, 
then decides if the item is equippable based on the
sub class' specific criteria for equipping items.

**ITEMS**

The **Item** class is the superclass of Armor and Weapon.
Items rely on several enums. 
The Slot enum which is used by both sub classes and
WeaponType and ArmorType which believe it or not is used
by the Weapon and Armor class respectively. 
These Enums have a name and a toString method,
which is not used - but the author wanted to freshen up on 
how to create enums with properties and constructors ;-)