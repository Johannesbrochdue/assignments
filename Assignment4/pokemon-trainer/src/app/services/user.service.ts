import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {CatalogueService} from "./catalogue.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor() {}

  public loginUser(username: string) {
    let users = UserService.getUsers()
    if (!users) {
      localStorage.setItem('users', JSON.stringify([]))
      users = JSON.parse(localStorage.getItem('users')!!)
    }
    let existingUser = this.getExistingUserOrFalse(username, users)
    if (existingUser) {
      localStorage.setItem('currentUser', JSON.stringify(existingUser))
    } else {
      let user = this.initUser(username)
      users.push(user)
      localStorage.setItem('users', JSON.stringify(users))
      localStorage.setItem('currentUser', JSON.stringify(user))
    }
  }

  public logoutUser() {
    this.saveCurrentUserToUsers()
    localStorage.setItem('currentUser', JSON.stringify(null))
  }

  public static noUser() {
    return !this.getCurrentUser();
  }

  public static getUsers() {
    let users = localStorage.getItem('users')!!
    return JSON.parse(users)
  }

  public static getCurrentUser() {
    let currentUser = localStorage.getItem('currentUser')!!
    return JSON.parse(currentUser)
  }

  public static getAcquiredPokemon() {
    return this.getCurrentUser().acquired_pokemon
  }

  public static updateAcquiredPokemon(pokemon: PokemonObject) {
    let {username, acquired_pokemon} = this.getCurrentUser()
    if (!pokemon.selected) {
      acquired_pokemon = acquired_pokemon.filter((it: PokemonObject) => {
        return (it.id != pokemon.id)
      })
    } else {
      acquired_pokemon.push(pokemon)
    }
    localStorage.setItem('currentUser', JSON.stringify({username, acquired_pokemon}))
  }

  private getExistingUserOrFalse(username: string, users: User[]) : any {
    let result = users.filter(it => it.username === username)
    return result !== [] ? result[0] : false
  }

  private initUser(username: string) {
    return {
      username: username,
      acquired_pokemon: []
    }
  }

  private saveCurrentUserToUsers() {
    let currentUser: User = UserService.getCurrentUser()
    let users: User[] = UserService.getUsers()
    let filteredUsers = users.filter(user => {
      return user.username !== currentUser.username
    })
    filteredUsers.push(currentUser)
    localStorage.setItem('users', JSON.stringify(filteredUsers))
  }
}
