import {Component, OnInit} from "@angular/core";
import {CatalogueService} from "../services/catalogue.service";
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {
  constructor(private readonly catalogueService: CatalogueService,
              private readonly userService: UserService,
              private readonly router: Router) {}

  // @ts-ignore
  ngOnInit() {
    if (UserService.noUser()) {
      return this.router.navigate(['login'])
    }
    this.catalogueService.fetchPokemons()
  }

  get pokemons(): PokemonObject[] {
    return this.catalogueService.getPokemons()
  }

  public logout() {
    this.userService.logoutUser()
    return this.router.navigate(['login'])
  }

  public loadMore(): void {
    this.catalogueService.fetchPokemons()
  }
}
