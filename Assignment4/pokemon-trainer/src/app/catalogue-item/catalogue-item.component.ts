import {Component, Input} from "@angular/core";
import {CatalogueService} from "../services/catalogue.service";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-catalogue-item',
  templateUrl: './catalogue-item.component.html',
  styleUrls: ['./catalogue-item.component.css']
})
export class CatalogueItemComponent {
  constructor(private readonly catalogueService: CatalogueService) {
  }
  @Input() pokemon : PokemonObject | undefined

  public selectPokemon(pokemon: PokemonObject | undefined) {
    pokemon!!.selected = !pokemon?.selected;
    UserService.updateAcquiredPokemon(pokemon!!)
    this.catalogueService.updatePokemonList(pokemon!!)
  }
}
