import {Component, OnInit} from "@angular/core";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";
import {CatalogueService} from "../services/catalogue.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
  constructor(private readonly userService: UserService,
              private readonly router: Router) {
  }



  // @ts-ignore
  ngOnInit() {
    if (UserService.noUser()) {
      return this.router.navigate(['login'])
    }
  }

  public logout() {
    this.userService.logoutUser()
    return this.router.navigate(['login'])
  }

  get selectedPokemon() {
    return UserService.getAcquiredPokemon()
  }

}
