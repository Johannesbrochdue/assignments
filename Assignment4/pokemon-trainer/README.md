# Pokemon Trainer

The application consists of two services. 

UserService and CatalogueService. 

The CatalogueService is responsible for providing
the catalogue of pokemon. 

The UserService is responsible for all user-related
functionality. 

I had some problems with the catalogue displaying
the previous user's "selected pokemon" when 
logging in. 

I got around it by expanding the "getPokemon" function
in CatalogueService to explicitly set the "selected" attribute
on the acquired pokemon in the current User. And unset the 
selected attribute for the other pokemon. It feels a little dirty and
frankly somewhat immoral. 
